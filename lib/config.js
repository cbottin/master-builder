var _ = require('lodash'),
    gulp,
    config;

config = {
    /**
     * The name of the coverage theme to use (only thunderhead is currently supported)
     */
    coverageTheme: 'thunderhead',

    /**
     * The root directory of the repository using this module
     */
    rootDir: null,

    /**
     * The directory where the output files (like coverage data files) are written
     */
    outputDir: null,

    /**
     * The directory where the HTML coverage reports for the modules are written
     */
    coverageModulesHtmlDir: null,

    /**
     * The directory where the JSON coverage reports for the modules are written
     */
    coverageModulesJsonDir: null,

    /**
     * The directory where the HTML coverage reports for the packages are written
     */
    coveragePackagesHtmlDir: null,

    /**
     * The directory where the JSON coverage reports for the packages are written
     */
    coveragePackagesJsonDir: null,

    /**
     * The directory where the Text coverage reports for the packages are written
     */
    coveragePackagesTextDir: null,

    /**
     * The object containing the configuration of the modules for the repository
     */
    modules: null,

    /**
     * @private
     */
    moduleAliases: null,

    /**
     * The object containing the configuration of the packages for the repository
     */
    packages: null,

    /**
     * @private
     */
    packageAliases: null,

    /**
     * The default test configuration (only frameworks, logLevel are currently supported)
     */
    testConfig: null,

    /**
     * The scope used when building the application (TODO: still needed?)
     */
    appScope: 'DEVELOPMENT-IDM', //DEVELOPMENT-IDM, DEVELOPMENT-NO-IDM, DEVELOPMENT

    /**
     * Temporary property: Location of the ANT master build file
     */
    antMasterBuildFile: null,

    /**
     * Temporary property: Location of the MSD build file
     */
    antMSDBuildFile: null,

    /**
     * Location of the Tomcat Bin directory
     */
    tomcatBinPath: null

};

function getGulp() {
    return gulp;
}

function setGulp(userGulp) {
    gulp = userGulp;
}

module.exports = {

    config: config,

    setConfig: function(userConfig) {
        _.merge(config, userConfig);

        config.moduleAliases = {};

        Object.keys(config.modules).forEach(function(moduleName) {
            config.modules[moduleName].name = moduleName; // add dynamic property 'name' to the object
            if (config.modules[moduleName].alias) {
                config.moduleAliases[config.modules[moduleName].alias] = moduleName;
            }
        });

        config.packageAliases = {};

        Object.keys(config.packages).forEach(function(packageName) {
            config.packages[packageName].name = packageName; // add dynamic property 'name' to the object
            if (config.packages[packageName].alias) {
                config.packageAliases[config.packages[packageName].alias] = packageName;
            }
        });
    },

    getGulp: getGulp,
    setGulp: setGulp
};