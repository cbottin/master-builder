var path = require('path'),
    gulp = require('gulp'),
    _ = require('lodash'),
    async = require('async'),
    config = require('../config.js').config;

/**
 * Make an union of two objects into an array.
 *
 * @param {String|String[]} currentList
 * @param {String|String[]} newList
 * @return {String[]} a array containing distinct items
 */
function unionObjects(currentList, newList) {
    if (!_.isArray(newList)) {
        newList = [newList];
    }
    if (!_.isArray(currentList)) {
        currentList = [currentList];
    }
    return _.union(currentList, newList);
}

/**
 * Gets a list of dependency files for the given package.
 *
 * @param {Object} pkg the package object
 * @param {function(files[])} callback The function to execute when all the dependencies have been fecthed
 */
function getPackageDependencyFiles(pkg, callback) {

    if (!pkg.dependencies) {
        callback([]);
        return;
    }

    var files = [];

    async.eachSeries(pkg.dependencies, function(dependency, seriesCallback) {

        dependency = dependency.split('.');

        var dependentPackage,
            depName = dependency[0],
            depType = dependency[1]; // locale, css, output or undefined (default = output)

        if (depName === 'self') {
            // get a specific type from self package
            if (pkg[depType]) {
                files = unionObjects(files, pkg[depType]);
                seriesCallback();
            } else {
                throw 'Cannot find property [' + depType + '] inside package [' + name + ']';
            }
        } else {

            dependentPackage = config.packages[depName];

            if (dependentPackage === pkg) {
                throw 'Package [' + name + '] cannot depend on itself';
            }

            if (!dependentPackage) {
                throw 'Cannot find package [' + depName + ']';
            }

            if (depType) {
                // get a specific type from other package
                if (dependentPackage[depType]) {
                    files = unionObjects(files, dependentPackage[depType]);
                    seriesCallback();
                } else {
                    throw 'Cannot find property [' + depType + '] inside package [' + depName + ']';
                }
            } else {

                // get dependencies from other package
                getPackageDependencyFiles(dependentPackage, function (dependencyFiles) {

                    files = unionObjects(files, dependencyFiles);

                    if (dependentPackage.src) {
                        getPackageSourceFiles(dependentPackage, function (dependentPackageSourceFiles) {
                            files = unionObjects(files, dependentPackageSourceFiles);
                            seriesCallback();
                        });
                    }
                    else if (dependentPackage.output) {
                        files = unionObjects(files, dependentPackage.output);
                        seriesCallback();
                    }
                    else {
                        throw 'Cannot find dependency files for [' + depName + ']';
                    }
                });
            }
        }
    }, function() {
        callback(files);
    });
}

/**
 * ASYNC: Gets a list of source files for the given package in order of dependencies.
 *
 * @param {Object} pkg Reference to the package object
 * @param {function} callback The function to execute when the source files are retrieved.
 */
function getPackageSourceFiles(pkg, callback) {

    var depsOrder = require('gulp-deps-order'),
        files = [],
        sourcePaths;

    sourcePaths = normalizePaths(pkg.src).map(function(srcPath) {
        return path.join(config.rootDir, srcPath, '**/*.js');
    });

    gulp.src(sourcePaths)
        .pipe(depsOrder({
            verbose: false,
            annotation: 'thdependency',
            dependencyFilePathResolver: function(dependency, filePath) {

                dependency = dependency.trim();

                var namespaces,
                    dependencyPath;

                if (_.isString(pkg.namespace)) {
                    namespaces = [{
                        namespace: pkg.namespace,
                        path: pkg.src
                    }];
                } else {
                    namespaces = Object.keys(pkg.namespace).map(function(namespace) {
                        return {
                            namespace: namespace,
                            path: pkg.namespace[namespace]
                        }
                    });
                }

                namespaces.forEach(function(ns) {

                    var namespace = ns.namespace + '.';

                    // ensure the dependency starts with the right namespace
                    if (dependency.substring(0, namespace.length) === namespace) {
                        dependencyPath = path.join(config.rootDir, ns.path, dependency.substring(namespace.length).replace(/\./g, path.sep) + '.js');
                    }

                });

                if (!dependencyPath) {
                    throw new Error('Invalid dependency found: ' + dependency + ' in file ' + filePath);
                }

                return dependencyPath;
            }
        }))
        .on('data', function(file) {
            files.push(file.path)
        })
        .on('end', function() {
            callback(files);
        });
}

/**
 * ASYNC: Gets a list of all the files required by the package in order to run the unit tests.
 *
 * @param {Object} pkg Reference to the package object
 * @param {function} callback The function to execute when the test files are retrieved.
 */
function getTestFiles(pkg, callback) {

    getPackageSourceFiles(pkg, function (files) {

         getPackageDependencyFiles(pkg, function(dependencyFiles) {

             var packageTestFiles,
                 testLib,
                 testPaths,
                 testAssetPaths;

             packageTestFiles = dependencyFiles.concat(files);

             testLib = pkg['test-lib'];

             if (testLib) {
                 normalizePaths(testLib).forEach(function (testLibPath) {
                     packageTestFiles.push(path.join(config.rootDir, testLibPath));
                 });
             }

             testPaths = normalizePaths(pkg.test).map(function (testPath) {
                 return path.join(config.rootDir, testPath, '**/*.js');
             });

             packageTestFiles = packageTestFiles.concat(testPaths);

             testAssetPaths = normalizePaths(pkg.test).map(function (testPath) {
                 return {
                     pattern: path.join(config.rootDir, testPath, '**/*.{json,xml,txt,html,png,gif,jpg,jpeg}'),
                     watched: true,
                     served: true,
                     included: false
                 }
             });

             packageTestFiles = packageTestFiles.concat(testAssetPaths);

             callback(packageTestFiles);
         });
    });
}

/**
 * Normalize the paths into an array if not already
 *
 * @param {String|String[]} paths The reference to the package
 * @returns {String[]} List of source paths
 */
function normalizePaths(paths) {
    return _.isString(paths) ? [paths] : paths;
}

module.exports = {
    getTestFiles: getTestFiles,
    getPackageSourceFilesInOrder: getPackageSourceFiles,
    normalizePaths: normalizePaths
};
