var path = require('path'),
    async = require('async'),
    colors = require('colors/safe'),
    gulp = require('gulp'),
    _ = require('lodash'),
    packageFiles = require('./files.js'),
    testCoverage = require('../test/coverage/index.js'),
    testRunner = require('../test/runner/index.js'),
    config = require('../config.js').config;


/**
 * Run the unit tests for all the packages.
 *
 * @param {Config} options The config options
 *  @config {Boolean} failOnError True if the command should report a failure when the unit tests fail.
 *  @config {Boolean} generateMaps True if the coverage maps data should be created (coverage data with only statement, function and branches maps info)
 *  @config {Object} [client={}] An object which will be passed in to the test runner framework
 *  @config {Boolean} [noCoverage=false] True if no coverage report should be created
 * @param {function(error)} done The function to execute when the run is completed
 */
function testAll(options, done) {

    options = {
        failOnError: options.failOnError,
        noCoverage: options.noCoverage,
        client: options.client,
        coverageReportName: 'all',
        copyPackageDataToModule: false
    };

    testSome(Object.keys(config.packages), options, done);
}

/**
 * Run the unit tests for the specified packages
 *
 * @param {String[]} packageNames Names of the package to include in the run
 * @param {Config} options The config options
 *  @config {Boolean} failOnError True if the command should report a failure when the unit tests fail.
 *  @config {Boolean} generateMaps True if the coverage maps data should be created (coverage data with only statement, function and branches maps info)
 *  @config {String} coverageReportName The name of the coverage report folder
 *  @config {Boolean} copyCoverageDataToModule True if the coverage data files for the packages should be copied to the module's folder
 *  @config {Object} [client={}] An object which will be passed in to the test runner framework
 *  @config {Boolean} [noCoverage=false] True if no coverage report should be created
 * @param {function(error)} done The function to execute when the run is completed
 */
function testSome(packageNames, options, done) {

    // filter only the packages containing test files
    packageNames = _.filter(packageNames, function(packageName) {
       return !!config.packages[packageName].test;
    });

    var coverageReportName = options.coverageReportName,
        copyCoverageDataToModule = options.copyCoverageDataToModule !== false,
        runUnitTestsPackageOptions = {
            generateMaps: options.generateMaps,
            autoWatch: false,
            client: options.client || {},
            noCoverage: (options.noCoverage === true)
        };

    cleanModuleCoverageReportDir(coverageReportName, function() {
        async.eachSeries(packageNames, function(packageName, seriesCallback) {
            test(packageName, runUnitTestsPackageOptions, function(error) {
                if (options.failOnError && error) {
                    seriesCallback(error);
                }
                else {
                    seriesCallback();
                }
            });
        }, function(err) {
            if (err) {
                done(true); // true to notify the calling function of a failure
            }
            else if (options.noCoverage) {
                done(false);
            }
            else {
                createModuleCoverageReport(coverageReportName, packageNames, copyCoverageDataToModule, function() {
                    done(false);
                });
            }
        });
    });
}

/**
 * Runs the unit tests from the given package.
 *
 * @param {String} packageNameOrAlias The name of the package or its alias
 * @param {Config} options The config options
 *  @config {Boolean} generateMaps True if the coverage maps data should be created (coverage data with only statement, function and branches maps info)
 *  @config {Boolean} [autoWatch=false] True if karma should be left open and listen to file changes to re-run the tests
 *  @config {Object} [client={}] An object which will be passed in to the test runner framework
 *  @config {Boolean} [noCoverage=false] True if no coverage report should be created
 * @param {function(error)} done The function to execute when the run is completed
 */
function test(packageNameOrAlias, options, done) {

    var pkg = getPackage(packageNameOrAlias),
        generateMaps = (options.generateMaps === true);

    options = {
        generateMaps: (options.generateMaps === true),
        autoWatch: (options.autoWatch === true),
        client: options.client || {},
        noCoverage: (options.noCoverage === true)
    };

    // If the package has no unit tests, don't proceed but don't report an error
    if (!pkg.test) {
        done(false);
        return;
    }

    cleanPackageCoverageReportDir(pkg.name, function() {
        getPackageTestRunnerConfig(pkg, options, function (testRunnerConfig) {

            console.log('================================================================================');
            console.log(colors.blue('Testing package:'), colors.bgBlue.black(pkg.name));

            testRunner.execute(testRunnerConfig, function (exitCode) {
                var error = (exitCode !== 0);
                if (options.noCoverage) {
                    done(error);
                } else {

                    var coverageFilePath = path.join(config.coveragePackagesJsonDir, pkg.name, 'coverage.json');

                    if (generateMaps) {
                        testCoverage.generateCoverageMapsFile(coverageFilePath);
                    }

                    testCoverage.createReport(coverageFilePath, path.join(config.coveragePackagesHtmlDir, pkg.name), config.rootDir, function() {
                        done(error);
                    });
                }
            });
        });
    });
}

/**
 * Gets a reference to a package object by using the package name or alias.
 *
 * @param {String} packageNameOrAlias The name of the package or its alias
 * @return {Object} the reference to the package object
 */
function getPackage(packageNameOrAlias) {

    var pkg,
        packageName = packageNameOrAlias;

    if (!config.packages[packageName]) {
        packageName = config.packageAliases[packageName];
    }

    pkg = config.packages[packageName];

    if (!pkg) {
        throw new Error(('Package [' + packageNameOrAlias + '] doesn`t exist').red);
    }

    return pkg;
}


/**
 * Gets a test runner config for a given package.
 *
 * @param {Object} pkg A reference to the package object
 * @param {Object} options Options for the test runner
 *  @config {Boolean} [autoWatch=false] True if karma should be left open and listen to file changes to re-run the tests
 *  @config {Object} [client={}] An object which will be passed in to the test runner framework
 *  @config {Boolean} [noCoverage=false] True if no coverage report should be created
 * @param {function(Object config)} done The callback function to execute when the config is ready
 */
function getPackageTestRunnerConfig(pkg, options, done) {

    options = {
        autoWatch: (options.autoWatch === true),
        client: options.client || {},
        noCoverage: (options.noCoverage === true)
    };

    packageFiles.getTestFiles(pkg, function(packageTestFiles) {

        var coveragePreprocessors = {},
            frameworks,
            logLevel,
            testRunnerConfig;

        packageFiles.normalizePaths(pkg.src).forEach(function(srcPath) {
            coveragePreprocessors[path.join(config.rootDir, srcPath, '**/*.js')] = ['coverage'];
        });

        if (pkg['test-frameworks']) {
            frameworks = pkg['test-frameworks'];
        } else if (config.testConfig && config.testConfig.frameworks) {
            frameworks = config.testConfig.frameworks;
        }

        if (config.testConfig && config.testConfig.logLevel) {
            logLevel = config.testConfig.logLevel;
        }

        testRunnerConfig = {
            frameworks: frameworks,
            logLevel: logLevel,
            preprocessors: options.noCoverage ? {} : coveragePreprocessors,
            reporters: options.noCoverage ? ['dots'] : ['dots', 'coverage'],
            coverageReporter: {
                reporters: [
                    {
                        type: 'json',
                        dir: config.coveragePackagesJsonDir,
                        subdir: pkg.name,
                        file: 'coverage.json'
                    },
                    {
                        type: 'text-summary',
                        dir: config.coveragePackagesTextDir,
                        subdir: pkg.name,
                        file: 'coverage.txt'
                    },
                    {
                        // this entry is to report the text coverage in the console
                        type: 'text-summary',
                        dir: config.coveragePackagesTextDir,
                        subdir: pkg.name
                    }
                ]
            },
            basePath: config.rootDir,
            files: packageTestFiles,
            singleRun: !options.autoWatch,
            autoWatch: options.autoWatch,
            client: options.client
        };

        done(testRunnerConfig);
    });
}


/**
 * Deletes existing coverage report directory for the given module report name.
 *
 * @param {String} reportName The name of the module report
 * @param {function} done The function to execute after clean up is finished
 */
function cleanModuleCoverageReportDir(reportName, done) {

    var rimraf = require('rimraf');

    rimraf(path.join(config.coverageModulesHtmlDir, reportName), function() {
        rimraf(path.join(config.coverageModulesJsonDir, reportName), function() {
            done();
        });
    });
}

/**
 * Deletes existing coverage report directory for the given package report name.
 *
 * @param {String} reportName The name of the package report
 * @param {function} done The function to execute after clean up is finished
 */
function cleanPackageCoverageReportDir(reportName, done) {

    var rimraf = require('rimraf');

    rimraf(path.join(config.coveragePackagesHtmlDir, reportName), function() {
        rimraf(path.join(config.coveragePackagesJsonDir, reportName), function() {
            rimraf(path.join(config.coveragePackagesTextDir, reportName), function() {
                done();
            });
        });
    });
}

/**
 * Creates a module coverage report from the list of given package names
 *
 * @param {String} reportName The name of the module report
 * @param {String[]} packageNames The list of package names
 * @param {Boolean} copyCoverageDataToModule True if the package data files should be copied to the module report directory
 * @param {function} done The function to execute after the report is created
 */
function createModuleCoverageReport(reportName, packageNames, copyCoverageDataToModule, done) {

    var files = [];
    var filesToCopy = [];

    packageNames.forEach(function (packageName) {
        if (packageName) {
            files.push(path.join(config.coveragePackagesJsonDir, packageName, 'coverage.json'));
            filesToCopy.push(path.join(config.coveragePackagesJsonDir, packageName, '*.json'));
        }
    });

    testCoverage.createReport(files, path.join(config.coverageModulesHtmlDir, reportName), config.rootDir, function () {
        if (copyCoverageDataToModule) {
            // Copy the JSON files inside the module folder so they can be copied to Integration easily
            // See ANT master-build.ci-publish-unit-tests-coverage-data
            gulp.src(filesToCopy, {base: config.coveragePackagesJsonDir})
                .pipe(gulp.dest(path.join(config.coverageModulesJsonDir, reportName)))
                .on('end', function() {
                    done();
                });
        } else {
            done();
        }
    });
}

module.exports = {
    test: test,
    testAll: testAll,
    testSome: testSome
};