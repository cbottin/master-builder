var path = require('path'),
    gulp = require('gulp'),
    colors = require('colors/safe'),
    _ = require('lodash'),
    packageFiles = require('../../packages/files.js'),
    config = require('../../config.js').config;

/**
 * Build the HTML coverage report from the given data at the given destination
 */
function createReport(files, destPath, srcDir, done) {

    var rimraf = require('rimraf'),
        async = require('async'),
        fs = require('fs'),
        verbose = process.argv.indexOf('--verbose') !== -1;

    if (_.isString(files)) {
        files = [files];
    }

    // delete the destination
    rimraf(destPath, function() {

        var istanbul = require('istanbul'),
            collector = new istanbul.Collector(),
            htmlReport,
            sourcePaths = {};

        _.forOwn(config.modules, function(module) {
            module.packages.forEach(function(pkgName) {
                var pkg = config.packages[pkgName],
                    srcPaths, rootname, basename;
                if (pkg.src) {
                    srcPaths = packageFiles.normalizePaths(pkg.src);
                    srcPaths.forEach(function(srcPath) {
                        rootname = module.alias || module.name;
                        basename = (srcPaths.length > 1) ? path.basename(srcPath) : '';
                        sourcePaths[path.join(srcPath, path.sep)] = path.join('ROOT', rootname.toUpperCase() + ' - ' + pkgName, basename, path.sep);
                    });
                }
            });
        });

        htmlReport = istanbul.Report.create('html', {
            dir: destPath,
            useNestedView: true,
            useConventionalSorting: true,
            substituteFilePath: function(filePath) {

                // sourcePaths:
                // categoriesEditor/src/js/categoriesCreator/
                // contentEditor/src/js/contentEditor/
                // resourceEditor/src/js/resourceEditor/
                // resourceEditor/src/js/resourceEditorUI/

                // srcDir:
                // /Users/name/Workspace/communicate/

                // filePath:
                // /Users/name/Workspace/communicate/categoriesEditor/src/js/categoriesCreator/components/AbstractFieldActionCell.js
                // /Users/cbottin/Workspace/communicate/resourceEditor/src/js/resourceEditorUI/Context.js

                var relativeFilePath = filePath.substring(path.join(srcDir, path.sep).length);

                // relativeFilePath:
                // categoriesEditor/src/js/categoriesCreator/components/AbstractFieldActionCell.js
                // resourceEditor/src/js/resourceEditorUI/Context.js

                _.forOwn(sourcePaths, function(packageNamePath, packageSrcPath) {
                    if (relativeFilePath.indexOf(packageSrcPath) === 0) {
                        // packageSrcPath:
                        // categoriesEditor/src/js/categoriesCreator/
                        // resourceEditor/src/js/resourceEditorUI/

                        // relativeFilePath:
                        // categoriesEditor/src/js/categoriesCreator/components/AbstractFieldActionCell.js
                        // resourceEditor/src/js/resourceEditorUI/Context.js

                        filePath = path.join(packageNamePath, relativeFilePath.substring(packageSrcPath.length));

                        // filePath:
                        // categoryEditor/components/AbstractFieldActionCell.js
                        // resourceEditor/resourceEditorUI/Context.js

                        return false;
                    }
                });

                return filePath;
            },
            watermarks: {
                statements: [70, 80],
                lines: [70, 80],
                functions: [70, 80],
                branches: [70, 80]
            }
        });

        if (verbose) {
            console.log(new Date().toLocaleTimeString(), 'Collecting', files.length, 'files for generating the report');
        }

        // _.forEach(files, function(f, i) {
        //     console.log(new Date().toLocaleTimeString(), 'Collecting file', i+1);
        //
        //     collector.add(fixCoveragePaths(JSON.parse(fs.readFileSync(f, 'utf8')), srcDir));
        //
        //     //collector.add(fixCoveragePaths(require(path.resolve(f)), srcDir));
        // }, this);

        var i = 1;
        var relativeRootRegEx = new RegExp(/\.\//g);
        var pathSeparatorRegEx = new RegExp(/[\/\\]/g);

        async.each(files, function(f, callback) {

            var fileIndex = i++;

            fs.readFile(f, {encoding: 'utf8'}, function(err, data) {
                if (verbose) {
                    console.log(new Date().toLocaleTimeString(), 'Processing file', fileIndex, f);
                }
                if (err) {
                    throw err;
                } else {

                    //data = fixCoveragePaths(data, srcDir); // might be too slow with large data

                    // EXPERIMENT FOR FASTER COVERAGE GENERATION WITH LARGE COVERAGE DATA
                    data = data.replace(relativeRootRegEx, path.join(srcDir, path.sep));

                    if (path.sep === '\\') { // windows paths must be escaped
                        data = data.replace(pathSeparatorRegEx, '\\\\');
                    }

                    data = JSON.parse(data);

                    collector.add(data);
                    callback();
                }
            });

        }, function(err) {

            if (verbose) {
                console.log(new Date().toLocaleTimeString(), 'Writing report');
            }

            htmlReport.on('done', function() {
                copyTheme(config.coverageTheme, destPath, function() {
                    console.log(colors.blue('HTML coverage report created:'), colors.bgBlue.black(destPath));
                    console.log('================================================================================');
                    done();
                });
            });

            htmlReport.writeReport(collector, false);

        });
    });
}

/**
 * Fix the paths of each entry as they are relative to the root folder and the current working directory is _karma
 * This will prevent getting a "file not found" error when istanbul tries to copy the source files in the report folder
 */
function fixCoveragePaths(data, srcDir) {

     var newData = {};

     // Fix the paths of each entry as they are relative to the root folder and the current working directory is _karma
     // This will prevent getting a "file not found" error when istanbul tries to copy the source files in the report folder
     _.forIn(data, function (value, key) {

         // convert => ./common/src/js/ui/Something.js
         // into => {srcDir}/common/src/js/ui/Something.js

         if (value.path && value.path.substring(0, 2) === './') { //for full coverage data
             var entryPath = path.join(srcDir, value.path.substring(2));
             newData[entryPath] = _.merge({path: entryPath}, value);
         } else if (key.substring(0, 2) === './') { // for compact coverage data
             newData[key] = _.merge({}, value);
         } else {
             newData[key] = _.merge({}, value);
         }
     });

     return newData;
}

/**
 * Copies the coverage report theme files to the given destination
 */
function copyTheme(theme, dest, done) {
    gulp.src(path.join(__dirname, 'themes', theme, '/**/*'))
        .pipe(gulp.dest(dest))
        .on('end', done);
}

function generateCoverageMapsFile(coverageFilePath) {

    var inputData,
        outputDir,
        outputFile,
        outputFileName,
        fileWriter;

    inputData = require(coverageFilePath);
    outputFileName = 'maps.json';
    outputDir = path.dirname(coverageFilePath);
    outputFile = path.resolve(outputDir, outputFileName);

    var istanbul = require('istanbul');
    var FileWriter = istanbul.FileWriter;
    fileWriter = new FileWriter(true);

    fileWriter.writeFile(outputFile, function (contentWriter) {
        var first = true;

        contentWriter.println('{');

        _.forIn(inputData, function (obj, key) {
            if (first) {
                first = false;
            } else {
                contentWriter.println(',');
            }
            contentWriter.write(JSON.stringify(key) + ':' + JSON.stringify({
                s: _.mapValues(obj.s, function() {
                    return 0;
                }),
                f: _.mapValues(obj.f, function() {
                    return 0;
                }),
                b: _.mapValues(obj.b, function(val) {
                    return _.map(val, function() {
                        return 0;
                    });
                }),
                path: obj.path,
                statementMap: obj.statementMap,
                branchMap: obj.branchMap,
                fnMap: obj.fnMap
            }));
        });

        contentWriter.println('}');
    });

    fileWriter.done();
}


module.exports = {
    createReport: createReport,
    generateCoverageMapsFile: generateCoverageMapsFile
};
