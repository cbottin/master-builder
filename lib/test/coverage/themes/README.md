#Istanbul HTML Coverage Report Themes

This is the location for storing the Istanbul HTML coverage report themes.

Each theme should be added as a sub-folder inside this `coverage-themes` older.

Istanbul doesn't offer option to use a custom theme so the workaround is to copy the theme's base.css (and any additional assets like fonts and images)
over to the folder containing the generated the coverage HTML report.

##To test

* `$ cd _karma`
* `$ gulp test-util`
* open _karma/coverage/html/Thunderhead-util/output/index.html in your browser