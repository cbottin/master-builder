/**
 * Unit tests should have special version of 'jQuery.event.special'.
 *
 * JQuery's methods {{ $(<element>).focus(); $(<element>).blur(); }}  call the native .blur() or .focus() functions
 * But they are not fired focus/blur events in unit-tests (because the window is not focused).
 */
if (Thunderhead.Browser.isChrome) {//the issue is actual for Chrome only
    jQuery.event.special.focus.trigger = function() {
        if (this !== document.activeElement) {
            $(document.activeElement).blur();
        }
    };

    jQuery.event.special.blur.trigger = function() {
        if (this.blur) {
            this.blur();
        }
    };
}
