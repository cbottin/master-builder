

jstestdriver.util.onAfterTestSetUp(function() {
    if (window.jQuery && jQuery.support) {
        // Turn off transition support so CSS animation are not used. Transition events are not firing in unit tests
        jQuery.support.__transition = jQuery.support.transition;
        jQuery.support.transition = null;
    }

    // Resolves an issue with tests failing in IE8 because the events don't fire.
    // For the issue see http://www.sencha.com/forum/showthread.php?139243-dom.style.setExpression-not-implemented-in-IE8
    if (window.Ext && window.Ext.isIE8) {
        Ext.supports.IncludePaddingInWidthCalculation = true;
        Ext.supports.IncludePaddingInHeightCalculation = true;
    }
});

jstestdriver.util.onAfterTestTearDown(function() {
    if (window.jQuery && jQuery.support) {
        jQuery.support.transition = jQuery.support.__transition;
        delete jQuery.support.__transition;
    }

    jstestdriver.util.assertVisibleDialogWindowCount(0);

    // TODO: instead of clearing the elements, we should add logic to check if elements
    // have been added but not removed during the test the test and throw an error "TIDY AFTER YOURSELF"!
    jstestdriver.util.clearElements();
});


/**
 * Assert specific property of the object
 * @param {String} [msg]
 * @param {Object} expected Expected object
 * @param {Object} actual Actual object
 * @param {Object} [options]
 *  @config {Number} [maxDepth] Maximum check depth
 *  @config {Number} [currentDepth]
 *  @config {String} propertyName Name of property to check
 */
function assertObjectProperty(msg, expected, actual, options) {
    jstestdriver.util.assertionWrapper(function(msg, expected, actual, options) {
        options.maxDepth = options.maxDepth || 20;
        options.currentDepth = options.currentDepth || 0;

        var actualProperty = actual[options.propertyName];
        var expectedProperty = expected[options.propertyName];
        var getType = function(obj) {
            return Object.prototype.toString.call(obj);
        };

        if (jstestdriver.util.isArray(expectedProperty)) {
            options.currentDepth++;
            assertArrayItems(msg, expectedProperty, actualProperty, options);
        } else if (jstestdriver.util.isObject(expectedProperty)) {
            options.currentDepth++;
            assertObjectProperties(msg, expectedProperty, actualProperty, options);
        } else if (getType(expectedProperty) === '[object Date]') {

            var actualType  = getType(actualProperty);

            if (actualType !== '[object Date]') {
                fail(msg + ' > [' + options.propertyName + '] type', '[object Date]', actualType);
            } else {
                assertSame(msg + ' > [' + options.propertyName + ']', expectedProperty.toString(), actualProperty.toString());
            }
        } else {
            assertSame(msg + ' > [' + options.propertyName + ']', expectedProperty, actualProperty);
        }
    }, arguments);
}

/**
 * Assert all properties of the object
 * @param {String} [msg]
 * @param {Object} expected Expected object
 * @param {Object} actual Actual object
 * @param {Object} [options]
 *  @config {Number} [maxDepth] Maximum check depth
 *  @config {Number} [currentDepth]
 */
function assertObjectProperties(msg, expected, actual, options) {
    jstestdriver.util.assertionWrapper(function(msg, expected, actual, options) {
        options.maxDepth = options.maxDepth || 20;
        options.currentDepth = options.currentDepth || 0;

        assertObject(msg + ' > Assert expectedObject', expected);
        assertObject(msg + ' > Assert actualObject', actual);

        if (expected === null || actual === null) {
            assertEquals(msg, expected, actual);
        }

        if (options.currentDepth >= options.maxDepth) {
            return;
        }

        var prop;
        for (prop in expected) {
            if (expected.hasOwnProperty(prop)) {
                options.propertyName = prop;
                assertObjectProperty(msg + '.' + options.propertyName, expected, actual, options);
            }
        }
    }, arguments);
}

/**
 * Assert items in arrays considering their order
 * @param {String} [msg]
 * @param {Object} expected Expected array
 * @param {Object} actual Actual array
 * @param {Object} [options]
 *  @config {Number} [maxDepth] Maximum check depth
 *  @config {Number} [currentDepth]
 */
function assertArrayItems(msg, expected, actual, options) {
    jstestdriver.util.assertionWrapper(function(msg, expected, actual, options) {
        options.maxDepth = options.maxDepth || 20;
        options.currentDepth = options.currentDepth || 0;

        assertArray(msg + ' > Assert expectedArray', expected);
        assertArray(msg + ' > Assert actualArray', actual);
        assertSame(msg + ' > Array Length', expected.length, actual.length);

        if (options.currentDepth >= options.maxDepth) {
            return;
        }

        var i, ii;
        for (i = 0, ii = expected.length; i < ii; i++) {
            options.propertyName = i;
            assertObjectProperty(msg + '[' + i + ']', expected, actual, options);
        }
    }, arguments);
}

/**
 * Asserts the attributes on two nodes.
 *
 * @param {String} [msg] Optional message/test title to prefix any failure message
 * @param {Object} expected The expected Node (element).
 * @param {Object} actual The actual Node (element).
 * @param {Object} [options]
 *  @config {String} expectedPath The path to the expected node, or just node name (used in failure messages)
 *  @config {String} actualPath The path to the actual node, or just node name (used in failure messages)
 */
function _assertXMLNodeAttributes(msg, expected, actual, options) {
    var expectedAttributes = expected.attributes;
    var actualAttributes = actual.attributes;
    if (!expectedAttributes) {
        assertSame(msg + 'Expected attributes undefined/null', expectedAttributes, actualAttributes);
        return;
    }
    var expectedAttributeNames = {};
    var i;
    for (i = 0; i < expectedAttributes.length; i++) {
        var expectedName = expectedAttributes.item(i).name;
        var actualAttr = actualAttributes.getNamedItem(expectedName);
        if (actualAttr === null || typeof actualAttr === 'undefined') {
            fail(msg + 'Node [' + options.actualPath + '] has missing expected attribute [' + expectedName + ']' +
                ((options.expectedPath !== options.actualPath) ? ' (found on expected Node [' + options.expectedPath + '])' : ''));
        }
        var expectedValue = expectedAttributes.item(i).value;
        var actualValue = actualAttr.value;
        if (expectedName === 'style' || expectedName === 'class') { // Seems perhaps a Chrome update now adds spaces for these...?
            expectedValue = expectedValue.replace(/^\s+/, '').replace(/\s+$/, '');
            actualValue = actualValue.replace(/^\s+/, '').replace(/\s+$/, '');
        }
        else if (expectedName === 'src') {
            actualValue = jstestdriver.util.replaceVersionPlaceholder(actualAttr.value);
        }
        if (expectedValue !== actualValue) {
            fail(msg + 'Node [' + options.expectedPath + '] Attribute [' + expectedName + '] expected value [' + expectedValue + '] but found ' +
                '[' + actualValue + ']' + ((options.expectedPath !== options.actualPath) ? ' on Node [' + options.actualPath + ']' : ''));
        }
        expectedAttributeNames[expectedName] = true;
    }

    if (actual.nodeType === 1 && expected.nodeType === 1) {
        var actualId = actual.getAttribute('id') || '';
        var expectedId = expected.getAttribute('id') || null;
        if (expectedId === null && actualId.indexOf('th-ce-block-') > -1) {
            actual.removeAttribute('id');
        }
    }

    if (expectedAttributes.length < actualAttributes.length) {
        var additionalAttributes = '';
        for (i = 0; i < actualAttributes.length; i++) {
            var actualAttribute = actualAttributes.item(i);
            if (!expectedAttributeNames[actualAttribute.name]) {
                additionalAttributes += ' [' + actualAttribute.name + '="' + actualAttribute.value + '"]';
            }
        }
        assertSame(msg + 'Node [' + options.actualPath + '] has extra attributes: ' + additionalAttributes +
            ((options.expectedPath !== options.actualPath) ? ' (not found on expected Node [' + options.expectedPath + '])' : ''),
            expectedAttributes.length, actualAttributes.length);
    }
}

/**
 * Asserts the text contained directly within the two nodes.
 * Number of text nodes are ignored, only the value is compared.
 * Also returns the number of expected & actual non-text nodes found
 * (object with two properties: expectedNodeCount & actualNodeCount).
 *
 * @param {String} [msg] Optional message text for prefixing any failure with
 * @param {Object} expected The expected Node (element).
 * @param {Object} actual The actual Node (element).
 * @param {Object} [options]
 *  @config {boolean} trim True if we should convert >1 consecutive space/tab to 1 space
 *  @config {boolean} ignoreNewLines True if we should ignore newlines (\r & \n)
 *  @config {String} expectedPath The path to the expected node, or just node name (used in failure messages)
 *  @config {String} actualPath The path to the actual node, or just node name (used in failure messages)
 * @returns {Object} result For information the number of non-text nodes within each element.
 */
function _assertXMLNodeText(msg, expected, actual, options) {
    var expectedText = '';
    var actualText = '';
    var expectedCount = 0;
    var actualCount = 0;
    var i;
    var child;
    // TODO - Requires improvement around newlines/space trimming
    for (i = 0; i < expected.childNodes.length; i++) {
        child = expected.childNodes[i];
        if (child.nodeType === 3) {
            expectedText += options.ignoreNewLines ? child.nodeValue.replace(/\n|\r|\t/g, '') : child.nodeValue;
            expectedCount++;
        }
    }
    for (i = 0; i < actual.childNodes.length; i++) {
        child = actual.childNodes[i];
        if (child.nodeType === 3) {
            actualText += options.ignoreNewLines ? child.nodeValue.replace(/\n|\r|\t/g, '') : child.nodeValue;
            actualCount++;
        }
    }
    // TODO - Trim leading/trailing whitespace if even only one char?
    expectedText = options.trim ? expectedText.replace(/(\s)\1+/g, '') : expectedText;
    actualText = options.trim ? actualText.replace(/(\s)\1+/g, '') : actualText;

    var message = '[' + options.expectedPath + ']';
    if (options.expectedPath !== options.actualPath) {
        msg += ' and [' + options.actualPath + ']';
    }
    assertSame(msg + 'Node text for ' + message, expectedText, actualText);
    return {
        expectedNodeCount: expected.childNodes.length - expectedCount,
        actualNodeCount: actual.childNodes.length - actualCount
    };
}

/**
 * Asserts two elements match (name, type and attributes).
 * If deep is true, will also recursively assert the children (and hence number of children is also checked).
 *
 * @param {String} [msg] Optional message/test title to prefix any failure message
 * @param {Object} expected The expected Node (element) (nodeType === 1).
 * @param {Object} actual The actual Node (element).
 * @param {Object} [options]
 *  @config {boolean} [deep] True if should recurse into the children.
 *  @config {boolean} [trim] True if we should convert >1 consecutive space/tab to 1 space
 *  @config {boolean} [ignoreNewLines] True if we should ignore newlines (\r & \n)
 *  @config {String} [path]
 */
function assertXMLNode(msg, expected, actual, options) {
    function assertFn(msg, expected, actual, options) {
        options.path = !options.path ? expected.nodeName : options.path + ' > ' + expected.nodeName;
        assertSame(msg + 'Node name (' + options.path + ')', expected.nodeName, actual.nodeName);
        assertSame(msg + 'Node type of [' + options.path + ']', expected.nodeType, actual.nodeType);
        _assertXMLNodeAttributes('', expected, actual, {
            expectedPath: options.path,
            actualPath: options.path
        });
        if (options.deep) {
            var textCompareResults = _assertXMLNodeText('', expected, actual, {
                trim: options.trim,
                ignoreNewLines: options.ignoreNewLines,
                expectedPath: options.path,
                actualPath: options.path
            });
            // Mixed mode, still compare any elements
            var expectedChildren = expected.childNodes.length;
            var actualChildren = actual.childNodes.length;
            assertSame(msg + 'Number children (excluding text nodes) for [' + options.path + ']',
                textCompareResults.expectedNodeCount, textCompareResults.actualNodeCount);
            // Only if we have non-text nodes to compare..
            if (textCompareResults.expectedNodeCount > 0) {
                // This method asserts elements in-order (important in some cases), could make this an option?
                var most = expectedChildren > actualChildren ? expectedChildren : actualChildren;
                var e = -1, a = -1;
                while (most-- > 0) { // If all text loop won't execute
                    e++;
                    a++;
                    // Skip past text nodes
                    while (e < expectedChildren && expected.childNodes[e].nodeType === 3) {
                        e++;
                    }
                    // (assertion above ensures we have the same # of non-text, so we can just use this to check if we've hit the end)
                    if (e === expectedChildren) {
                        most = 0; // Element node ended with text nodes
                    } else {
                        while (actual.childNodes[a].nodeType === 3) {
                            a++;
                        }
                        assertFn('', expected.childNodes[e], actual.childNodes[a], {
                            deep: options.deep,
                            trim: options.trim,
                            ignoreNewLines: options.ignoreNewLines
                        });
                    }
                }
            }
        }
    }

    jstestdriver.util.assertionWrapper(function(msg, expected, actual, options) {
        try {
            assertFn('', expected, actual, {
                deep: options.deep,
                trim: options.trim,
                ignoreNewLines: options.ignoreNewLines
            });
        }
        catch (ex) {
            try {
                jstestdriver.console.log(jstestdriver.util.formatXml(Thunderhead.util.XML.serialize(actual)));
            }
            catch (ex2) {
                // XML Serialization Error
            }
            fail(ex.message);
        }
    }, arguments);
}

/**
 * Asserts two elements match (name, type and attributes).
 * If deep is true, will also recursively assert the children (and hence number of children is also checked).
 *
 * @param {String} [msg] Optional message/test title to prefix any failure message
 * @param {Object} expected The expected Document.
 * @param {Object} actual The actual Document.
 * @param {Object} [options]
 *  @config {boolean} [trim] True if we should convert >1 consecutive space/tab to 1 space, default TRUE
 *  @config {boolean} [ignoreNewLines] True if we should ignore newlines (\r & \n), default TRUE
 */
function assertXMLDocument(msg, expected, actual, options) {
    jstestdriver.util.assertionWrapper(function(msg, expected, actual, options) {
        if (!options.trim) {
            options.trim = true;
        }
        if (!options.ignoreNewLines) {
            options.ignoreNewLines = true;
        }
        assertXMLNode(msg, expected.documentElement, actual.documentElement, {
            deep: true,
            trim: options.trim,
            ignoreNewLines: options.ignoreNewLines
        });
    }, arguments);
}


/**
 * Function to test the equality of HTML Elements
 *
 * @param {String} [msg] Optional message/test title to prefix any failure message
 * @param {HTMLElement} expected Expected HTML Element
 * @param {HTMLElement} actual Actual HTML Element
 * @param {Object} [options]
 *  @config {String} [path]
 *  @config {Number} [elementIndex]
 *  @config {boolean} [skipCheckStyles=true]
 *  @config {boolean} [skipCheckAttributes=true]
 */
function assertHTMLElements(msg, expected, actual, options) {
    function getChildElements(element) {
        var children = [];
        var i;
        var ii;
        for (i = 0, ii = element.childNodes.length; i < ii; i++) {
            children.push(element.childNodes[i]);
        }
        return children;
    }

    jstestdriver.util.assertionWrapper(function(msg, expected, actual, options) {
        options.path = (options.path ? options.path + '>' : '') + expected.tagName + '[' + (options.elementIndex ? options.elementIndex : 0) + ']';
        options.skipCheckStyles = (options.skipCheckStyles !== false); //skip check styles by default
        options.skipCheckAttributes = (options.skipCheckAttributes !== false); // skip attributes by default

        var expectedChildren = getChildElements(expected);
        var actualChildren = getChildElements(actual);

        assertEquals(options.path + ': children length', expectedChildren.length, actualChildren.length);
        assertEquals(options.path + ': className', expected.className, actual.className);

        if (!options.skipCheckStyles) {
                    $.each(expected.style, function( index, styleName ) {
                        assertEquals(options.path + ': "' + styleName + '" style', expected.style[styleName], actual.style[styleName]);
                    });
                }

        if (!options.skipCheckAttributes) {
            $.each(expected.attributes, function( index, attr ) {
                if (attr.name !== 'style') {
                    assertEquals(options.path + ': "' + attr.name + '" attribute', attr.value, $(actual).attr(attr.name));
                }
            });
        }

        var i;
        var ii;
        for (i = 0, ii = expectedChildren.length; i < ii; i++) {
            if (expectedChildren[i].nodeType === 1) { // element node
                assertHTMLElements(expectedChildren[i], actualChildren[i], {
                    path: options.path,
                    elementIndex: i,
                    skipCheckStyles: options.skipCheckStyles,
                    skipCheckAttributes: options.skipCheckAttributes
                });
            }
            else if (expectedChildren[i].nodeType === 3) {
                assertEquals('Text Node at ' + i, expectedChildren[i].nodeValue, actualChildren[i].nodeValue);
            }
        }
    }, arguments);
}

/**
 * Asserts the given HTML string matches that contained within the given node.
 *
 * @param {String} [msg] Optional message/test title to prefix any failure message
 * @param {String} expectedHTML The expected HTML string
 * @param {HTMLElement} actualNode The actual HTML node
 */
function assertInnerHTML(msg, expectedHTML, actualNode) {
    if (typeof actualNode === 'undefined') {
        actualNode = expectedHTML;
        expectedHTML = msg;
        msg = null;
    }

    var _htmlTestExpectedElement = document.createElement('div');
    var _htmlTestActualElement = document.createElement('div');

    _htmlTestExpectedElement.innerHTML = expectedHTML;
    _htmlTestActualElement.innerHTML = actualNode.innerHTML;

    try {
        assertXMLNode('', _htmlTestExpectedElement, _htmlTestActualElement, {
            deep: true
        });
    }
    catch (ex) {
        throw new Error((msg || 'assertInnerHTML') + ' > ' + ex.message);
    }
}