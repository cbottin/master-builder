function jstd_extend(d,a){var c;for(c in d){if(d.hasOwnProperty(c)){a[c]=d[c];}}return a;};

jstd_extend({

    /**
     * load url and return it contents
     *
     * @param {String} url file location to load
     */
    loadTextFile: function(url) {
        var xmlDoc = new XMLHttpRequest();
        xmlDoc.open('GET', url, false);
        xmlDoc.send(null);
        return xmlDoc.responseText;
    },

    /**
     * fire attached Event
     *
     * @param {String} eventName Name of the event to trigger
     * @param {HTMLElement} dom Element to trigger event for
     */
    fireEvent: (function() {
        if (document.fireEvent) {
            return function(eventName, dom) {
                dom.fireEvent('on' + eventName, document.createEventObject());
            };
        } else {
            return function(eventName, dom) {
                var event = document.createEvent("MouseEvent");
                event.initMouseEvent(eventName, true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
                dom.dispatchEvent(event);
            };
        }
    }()),

    /**
     * Fire attached keyboard event (keydown for example)
     * Supports IE8,IE9, Chrome, Firefox
     *
     * @param {HTMLElement} dom Element to trigger event for
     * @param {String} eventName Name of the event to trigger
     * @param {String} keyCode key code of the event
     * @param {boolean} altKey if ALT key is pressed
     * @param {boolean} ctrlKey if CTRL key is pressed
     * @param {boolean} shiftKey if SHIFT key is pressed
     * (can be enhanced later to support ctrl, meta and shift keys)
     */
    fireKeyboardEvent: function(dom, eventName, keyCode, altKey, ctrlKey, shiftKey) {
        if (Thunderhead.Browser.isIE8) {
            var e = document.createEventObject("KeyboardEvent");
            e.keyCode = keyCode;
            e.altKey = altKey;
            e.ctrlKey = ctrlKey;
            e.shiftKey = shiftKey;
            dom.fireEvent("on" + eventName, e);
            return;
        }
        var event = document.createEvent("KeyboardEvent");

        // Chromium Hack
        if (Thunderhead.Browser.isChrome) {
            Object.defineProperty(event, 'keyCode', {
                get: function() {
                    return this.keyCodeVal;
                }
            });

            Object.defineProperty(event, 'which', {
                get: function() {
                    return this.keyCodeVal;
                }
            });

            event.keyCodeVal = keyCode;
        }

        if (event.initKeyboardEvent) {
            if (Thunderhead.Browser.isIE9 || Thunderhead.Browser.isIE10) {
                // IE9 Hack
                Object.defineProperty(event, 'keyCode', {
                    get: function() {
                        return Number(this.key);
                    }
                });

                var modifiersListArg = '';
                if (altKey) {
                    modifiersListArg += ' Alt';
                }
                if (ctrlKey) {
                    modifiersListArg += ' Control';
                }
                if (shiftKey) {
                    modifiersListArg += ' Shift';
                }
                event.initKeyboardEvent(eventName, true, true, document.defaultView, keyCode, false, modifiersListArg,
                    false, '');
            } else {
                event.initKeyboardEvent(eventName, true, true, document.defaultView, keyCode, keyCode, ctrlKey, altKey,
                    shiftKey, false, false);
            }
        } else {
            event.initKeyEvent(eventName, true, true, document.defaultView, ctrlKey, altKey, shiftKey, false, keyCode, 0);
        }

        dom.dispatchEvent(event);
    }

}, jstestdriver);

function ExtendedTestCase(testCaseName, basePrototype, prototype) {

    var proto = Thunderhead.util.Objects.merge(basePrototype, prototype, false);
    proto.superclass = basePrototype;

    TestCase(testCaseName, proto);
}

function ExtendedAsyncTestCase(testCaseName, basePrototype, prototype) {

    var proto = Thunderhead.util.Objects.merge(basePrototype, prototype, false);
    proto.superclass = basePrototype;

    AsyncTestCase(testCaseName, proto);
}


// jstestdriver.plugins.async.DeferredQueueArmor => th-jstestdriver version
// jstestdriver.plugins.async.DeferredQueueDelegate => karma-jstd-adapter

var jstdAsyncQueueDelegate = jstestdriver.plugins.async.DeferredQueueArmor || jstestdriver.plugins.async.DeferredQueueDelegate;

if (jstdAsyncQueueDelegate) {

    jstdAsyncQueueDelegate.prototype.step = function(stepName, fn, scope) {
        this.call(stepName, function () {
            fn.call(scope);
        });
    };

    jstdAsyncQueueDelegate.prototype.pause = function(milliseconds) {
        this.call('Pause', function (callbacks) {
            setTimeout(callbacks.add(function () {}), milliseconds);
        });
    };
}
