jstestdriver.util.onBeforeTestCaseAdd(function(testCase) {
    if (testCase.JSTESTDRIVER_EXCLUDE_ALL === true) {
        var testName;
        for (testName in testCase) {
            if (testCase.hasOwnProperty(testName) && testName.indexOf('test') === 0) {
                delete testCase[testName];
            }
        }
    }
});