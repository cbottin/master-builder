var path = require('path');

var createPattern = function(path) {
    return {pattern: path, included: true, served: true, watched: false};
};

var initJSTD = function(files) {
    files.unshift(createPattern(__dirname + '/assertions.js'));
    files.unshift(createPattern(__dirname + '/exclusion.js'));
    files.unshift(createPattern(__dirname + '/extensions.js'));
    files.unshift(createPattern(__dirname + '/util.js'));
    files.unshift(createPattern(__dirname + '/jstd-adapter.js'));
};

initJSTD.$inject = ['config.files'];

module.exports = {
    'framework:jstd': ['factory', initJSTD]
};
