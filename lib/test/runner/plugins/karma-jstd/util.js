(function(context) {

    var jstestdriver = context.jstestdriver = context.jstestdriver || {};
    var events = {
        'onBeforeTestCaseAdd': {init: false, handlers: []},
        'onAfterTestCaseAdd': {init: false, handlers: []},
        'onBeforeTestTearDown': {init: false, handlers: []},
        'onAfterTestTearDown': {init: false, handlers: []},
        'onBeforeTestSetUp': {init: false, handlers: []},
        'onAfterTestSetUp': {init: false, handlers: []}
    };

    function interceptBefore(target, fnName, interceptFn) {
        var fn = target[fnName] || (function() {});
        return target[fnName] = function() {
            var result = interceptFn.apply(this, arguments);
            fn.apply(this, arguments);
            return result;
        };
    }

    function interceptAfter(target, fnName, interceptFn) {
        var fn = target[fnName] || (function() {});
        return target[fnName] = function() {
            fn.apply(this, arguments);
            return interceptFn.apply(this, arguments);
        };
    }

    function onTestCaseAdd(when, handler) {

        var event = events[when];
        var interceptor = (when === 'onBeforeTestCaseAdd') ? interceptBefore : interceptAfter;

        if (!event.init) {
            interceptor(jstestdriver.TestCaseManager.prototype, 'add', function (b) {
                var testCase = b.template_.prototype;
                forEachItem(event.handlers, function (fn) {
                    fn(testCase);
                });
            });
        }

        event.handlers.push(handler);
    }

    function onBeforeTestCaseAdd(handler) {
        onTestCaseAdd('onBeforeTestCaseAdd', handler);
    }

    function onAfterTestCaseAdd(handler) {
        onTestCaseAdd('onAfterTestCaseAdd', handler);
    }

    function onTestEvent(when, handler) {

        var event = events[when];
        var fnName = (when === 'onBeforeTestTearDown' || when === 'onAfterTestTearDown') ? 'tearDown' : 'setUp';
        var interceptor = (when === 'onBeforeTestTearDown' || when === 'onBeforeTestSetUp') ? interceptBefore : interceptAfter;

        if (!event.init) {
            onBeforeTestCaseAdd(function (testCase) {
                interceptor(testCase, fnName, function () {
                    forEachItem(event.handlers, function (fn) {
                        fn(testCase);
                    });
                });
            });
        }

        event.handlers.push(handler);
    }

    function onBeforeTestTearDown(handler) {
        onTestEvent('onBeforeTestTearDown', handler);
    }

    function onAfterTestTearDown(handler) {
        onTestEvent('onAfterTestTearDown', handler);
    }

    function onBeforeTestSetUp(handler) {
        onTestEvent('onBeforeTestSetUp', handler);
    }

    function onAfterTestSetUp(handler) {
        onTestEvent('onAfterTestSetUp', handler);
    }

    function isObject(obj) {
        return Object.prototype.toString.call(obj) === '[object Object]';
    }

    function isString(obj) {
        return Object.prototype.toString.call(obj) === '[object String]';
    }

    function isArray(obj) {
        return Object.prototype.toString.call(obj) === '[object Array]';
    }

    function assertionWrapper(callback, args) {
        var sameType = function(obj1, obj2) {
            return (typeof obj1 === typeof obj2);
        };

        var isObject = jstestdriver.util.isObject;
        var isString = jstestdriver.util.isString;

        if (args.length === 1 && isObject(args[0])) {
            // Call is assertSomething(options)
            callback.call(this, '', undefined, null, args[0]);
        } else if (args.length === 2) {
            if (isString(args[0]) && isObject(args[1])) {
                // Call is assertSomething(message, options)
                callback.call(this, args[0], undefined, null, args[1]);
            }
            if (sameType(args[0], args[1])) {
                // Call is assertSomething(expected, actual)
                callback.call(this, '', args[0], args[1], {});
            } else {
                fail('Error: incorrect arguments for assert call');
            }
        } else if (args.length === 3) {
            if (sameType(args[0], args[1]) && isObject(args[2])) {
                // Call is assertSomething(expected, actual, options)
                callback.call(this, '', args[0], args[1], args[2]);
            } else if (isString(args[0]) && sameType(args[1], args[2])) {
                // Call is assertSomething(message, expected, actual)
                callback.call(this, args[0], args[1], args[2], {});
            } else {
                fail('Error: incorrect arguments for assert call');
            }
        } else if (args.length === 4 && isString(args[0]) && sameType(args[1], args[2]) && isObject(args[3])) {
            // Call is assertSomething(message, expected, actual, options)
            callback.apply(this, args);
        } else {
            fail('Error: incorrect number of arguments for assert call');
        }
    }

    function forEachItem(array, callback, scope) {
        var value;
        var i, ii;

        if (array && array.length > 0) {
            for (i = 0, ii = array.length; i < ii; i++) {
                value = callback.call(scope || array[i], array[i], i);
                if (value === false) {
                    return i;
                }
            }
        }
        return -1;
    }

    function formatXml(xml) {
        var formatted = '';
        var reg = /(>)(<)(\/*)/g;
        xml = xml.replace(reg, '$1\r\n$2$3');
        var pad = 0;
        jstestdriver.util.forEachItem(xml.split('\r\n'), function(node) {
            var indent = 0;
            if (node.match(/.+<\/\w[^>]*>$/)) {
                indent = 0;
            } else if (node.match(/^<\/\w/)) {
                if (pad !== 0) {
                    pad -= 1;
                }
            } else if (node.match(/^<\w[^>]*[^\/]>.*$/)) {
                indent = 1;
            } else {
                indent = 0;
            }

            var padding = '';
            var i;
            for (i = 0; i < pad; i++) {
                padding += '  ';
            }

            formatted += padding + node + '\r\n';
            pad += indent;
        });

        return formatted;
    }

    /**
     * Updates given content, replacing any version number with the placeholder
     */
    function replaceVersionPlaceholder(content) {
        if (content) {
            return content.replace(/common(\/v-[0-9A-Za-z_.\-]+)?\/themes/, 'common/' + window.th_cacheKey + '/themes');
        }
        return content;
    }

    /**
     * Assert Ext dialogs count
     * @param {Number} expectedCount
     */
    function assertVisibleDialogWindowCount(expectedCount) {
        var count = 0;
        if (window.Ext) {
            window.Ext.WindowMgr.each(function(win) {
                if (!win.hidden) {
                    jstestdriver.console.log('The window "' + win.id + '" (' + win.title + ') is still opened');
                    count++;
                }
            });
        }
        assertSame('Visible Dialog Window Count', expectedCount, count);
        jstestdriver.assertCount--; // ensure we are not changing the number of expected asserts in the tests
    }

    function clearElements() {
        $(document.body).children().each(function (i, el) {
            if (el.tagName !== 'SCRIPT' && el.tagName !== 'LINK') {
                document.body.removeChild(el);
            }
        });
    }


    jstestdriver.util = {
        interceptBefore: interceptBefore,
        interceptAfter: interceptAfter,
        onBeforeTestCaseAdd: onBeforeTestCaseAdd,
        onAfterTestCaseAdd: onAfterTestCaseAdd,
        onBeforeTestTearDown: onBeforeTestTearDown,
        onAfterTestTearDown: onAfterTestTearDown,
        onBeforeTestSetUp: onBeforeTestSetUp,
        onAfterTestSetUp: onAfterTestSetUp,
        isArray: isArray,
        isString: isString,
        isObject: isObject,
        assertionWrapper: assertionWrapper,
        forEachItem: forEachItem,
        formatXml: formatXml,
        replaceVersionPlaceholder: replaceVersionPlaceholder,
        assertVisibleDialogWindowCount: assertVisibleDialogWindowCount,
        clearElements: clearElements
    };

})(window);