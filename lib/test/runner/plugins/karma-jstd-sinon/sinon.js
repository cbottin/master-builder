(function(context) {

    'use strict';

    var jstd = context.jstd = context.jstd || {};
    var spyList = [];
    var stubList = [];

    /**
     * @see sinon.spy
     * @throws Error if a target property is specified which is not null or a function
     */
    function spy() {
        checkArguments(arguments, 'spy');
        var spy = sinon.spy.apply(sinon, arguments);
        spyList.push(spy);
        return spy;
    }

    /**
     * @see sinon.stub
     * @throws Error if a target property is specified which is not null or a function
     */
    function stub() {
        checkArguments(arguments, 'stub');
        var stub = sinon.stub.apply(sinon, arguments);
        stubList.push(stub);
        return stub;
    }

    /**
     * Checks the arguments passed to spy/stub to ensure if targetting
     * a property that the existing property is indeed a function.
     *
     * @throws Error if a target property is specified which is not null or a function
     *
     * @param {Array} args "arguments" reference
     * @param {String} functionName For debug/error message (i.e. "spy" or "stub")
     */
    function checkArguments(args, functionName) {
        if (args.length >= 2) {
            var obj = args[0];
            var propKey = args[1];
            var spyProp = obj[propKey];
            if (spyProp !== null && !_.isFunction(spyProp)) {
                throw Error('Attempted jstd.sinon.' + functionName + ' of property "' + propKey + '" - which is not a function, is type [' + getType(spyProp) + ']');
            }
        }
    }

    /**
     * Returns a human readable type of the given value.
     *
     * @param {*} value The value to check
     * @return {String} Human readable/comparable type name
     */
    function getType(value) {
        if (value === null) {
            return 'null';
        }
        if (_.isArray(value)) {
            return 'array';
        }
        return typeof value;
    }

    /**
     * Restores sinon spies and stubs to their original states.
     */
    function restore() {
        _.each(spyList, function(spy) {
            if (spy.restore) {
                spy.restore();
            }
        });
        _.each(stubList, function(stub) {
            if (stub.restore) {
                stub.restore();
            }
        });

        spyList.length = 0;
        stubList.length = 0;
    }

    if (typeof jstestdriver !== 'undefined') {
        jstestdriver.util.onAfterTestTearDown(function() {
            restore();
        });
    }

    jstd.sinon = {
        spy: spy,
        stub: stub,
        restore: restore
    };

})(window);