var path = require('path');

var createPattern = function(path) {
    return {pattern: path, included: true, served: true, watched: false};
};

var initSinon = function(files) {
    files.unshift(createPattern(__dirname + '/sinon.js'));
};

initSinon.$inject = ['config.files'];

module.exports = {
    'framework:jstd-sinon': ['factory', initSinon]
};
