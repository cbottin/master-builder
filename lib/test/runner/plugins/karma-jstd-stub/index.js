var path = require('path');

var createPattern = function(path) {
    return {pattern: path, included: true, served: true, watched: false};
};

var initStub = function(files) {
    files.unshift(createPattern(__dirname + '/stub.js'));
};

initStub.$inject = ['config.files'];

module.exports = {
    'framework:jstd-stub': ['factory', initStub]
};
