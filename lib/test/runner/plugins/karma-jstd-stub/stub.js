(function(context) {

    'use strict';

    var jstd = context.jstd = context.jstd || {};
    var stubList = [];

    /**
     * Overrides a target properties
     *
     * @throws Error if the property values are not null or the types do not match
     *
     * @param {Object} target The object to override
     * @param {Object|String} props The list of properties and their overridden values or the name of the property to override
     * @param {Object} [value] The value of the property to override if props is a string
     */
    function apply(target, props, value) {
        if (_.isString(props)) {
            var propName = props;
            props = {};
            props[propName] = value;
        }

        // look for existing target
        var item = stub(target);

        if (!item) {
            // create an entry if we don't have a entry for this target already
            item = {
                target: target,
                originalProps: {}
            };
            stubList.push(item);
        }

        // override the members on the target while saving the original values
        _.forIn(props, function(value, key) {
            compareTypes(target, key, value);
            if (!item.originalProps.hasOwnProperty(key)) { //allow multiple overwrites
                item.originalProps[key] = target[key];
            }
            target[key] = value;
        });
    }

    /**
     * Checks whether the given value exists as a property with the key "key" on target,
     * and if so also compares the types of the values, throwing an error if they do not match.
     *
     * @remarks A null value is taken as {*} as we cannot inspect type
     * @throws Error if the property values are not null or the types do not match
     *
     * @param {Object} target The target object
     * @param {String} key The property key name we are checking
     * @param {*} newValue The proposed new value for the property key on target
     */
    function compareTypes(target, key, newValue) {
        var targetValue = target[key];
        if (_.isUndefined(targetValue)) { // Stubbing a property that is not defined on the object
            throw Error('Attempted jstd.stub of undefined property [' + key + ']');
        }
        if (targetValue !== null && newValue !== null) { // Null could be any type (so allow), we can't unfortunately reflect to see documentation!
            var newValueType = getType(newValue);
            var targetValueType = getType(targetValue);

            if (targetValueType !== newValueType) {
                throw Error('Attempted jstd.stub of [' + targetValueType + '] property "' + key + '" with type [' + newValueType + ']');
            }
        }
    }

    /**
     * Returns a human readable type of the given value.
     *
     * @param {*} value The value to check
     * @return {String} Human readable/comparable type name
     */
    function getType(value) {
        if (value === null) {
            return 'null';
        }
        if (_.isArray(value)) {
            return 'array';
        }
        return typeof value;
    }

    /**
     * Restores all targets or a specific target or a specific property.
     *
     * @param {Object} [target] A specific object to restore
     * @param {String} [property] A specific property to restore
     */
    function restore(target, property) {
        if (target && property) {
            restoreProperty(target, property);
        } else if (target) {
            restoreTarget(target);
        }
        else {
            restoreAll();
        }
    }

    /**
     * Restores all targets with their original properties.
     */
    function restoreAll() {
        _.each(stubList, function(item){
            _.forIn(item.originalProps, function(value, key) {
                item.target[key] = value;
            });
        });
        stubList.length = 0;
    }

    /**
     * Restores a specific target with its original properties.
     *
     * @param {Object} target The object to restore
     */
    function restoreTarget(target) {
        var item = stub(target);
        if (item) {
            _.forIn(item.originalProps, function(value, key) {
                item.target[key] = value;
            });
        }
    }

    /**
     * Restores all targets with their original properties.
     *
     * @param {Object} target The object containing the property to restore
     * @param {String} property The property to restore
     */
    function restoreProperty(target, property) {
        var item = stub(target);
        if (item && item.originalProps.hasOwnProperty(property)) {
            item.target[property] = item.originalProps[property];
        }
    }

    /**
     * Returns the original value of a property for the given target.
     *
     * @param {Object} target The object containing the property
     * @param {String} property The property name
     * @return {Object|undefined} The original property value or undefined if not found
     */
    function original(target, property) {
        // look for existing target
        var item = stub(target);
        if (item && item.originalProps.hasOwnProperty(property)) {
            return item.originalProps[property];
        }
    }

    /**
     * Returns the stub reference to a target if the target has already been mocked.
     *
     * @param {Object} target The object to retrieve
     * @return {Object|undefined} The stub reference or undefined if not found
     */
    function stub(target) {
        // look for existing target
        return _.find(stubList, function(stub) {
            return stub.target === target;
        });
    }

    if (typeof jstestdriver !== 'undefined') {
        jstestdriver.util.onAfterTestTearDown(function() {
            restore();
        });
    }

    jstd.stub = {
        apply: apply,
        original: original,
        restore: restore
    };

})(window);