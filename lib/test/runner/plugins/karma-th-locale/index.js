var path = require('path');

var createPattern = function(path) {
    return {pattern: path, included: true, served: true, watched: false};
};

var initTHLocale = function(files) {
    files.unshift(createPattern(__dirname + '/th-locale.js'));
};

initTHLocale.$inject = ['config.files'];

module.exports = {
    'framework:th-locale': ['factory', initTHLocale]
};
