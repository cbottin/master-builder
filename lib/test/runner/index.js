var karma = require('karma').server,
    _ = require('lodash');

function execute(userConfig, callback) {

    // required config for karma
    var karmaDefaultConfig = require('./config.json');

    karmaDefaultConfig.plugins = [
        'karma-chai',
        'karma-coverage',
        'karma-chrome-launcher',
        'karma-sinon',
        'karma-mocha',
        require('./plugins/karma-jstd/index.js'),
        require('./plugins/karma-jstd-stub/index.js'),
        require('./plugins/karma-jstd-sinon/index.js'),
        require('./plugins/karma-th-locale/index.js')
    ];

    karma.start(_.defaults({}, userConfig, karmaDefaultConfig), callback);
}

module.exports = {
    execute: execute
};