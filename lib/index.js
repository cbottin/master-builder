var configManager = require('./config.js');

module.exports = {
    setConfig: configManager.setConfig,
    initGulpTasks: function(gulp) {
        configManager.setGulp(gulp);
        require('../gulpfile.js');
    }
};