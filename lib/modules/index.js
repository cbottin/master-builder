var path = require('path'),
    gulp = require('gulp'),
    async = require('async'),
    _ = require('lodash'),
    packages = require('../packages/index.js'),
    util = require('../util.js'),
    config = require('../config.js').config;

/**
 * Run all unit tests from the packages inside the module.
 *
 * @param {String} moduleName The name of the module or its alias
 * @param {Config} options The config options
 *  @config {Boolean} failOnError True if the command should report a failure when the unit tests fail.
 *  @config {Boolean} generateMaps True if the coverage maps data should be created (coverage data with only statement, function and branches maps info)
 *  @config {Boolean} [noCoverage=false] True if no coverage report should be created
 * @param {function(error)} done The function to execute when the run is complete
 */
function test(moduleName, options, done) {

    var module = getModule(moduleName);

    options = {
        failOnError: options.failOnError,
        generateMaps: options.generateMaps,
        noCoverage: options.noCoverage,
        client: options.client,
        coverageReportName: module.name,
        copyPackageDataToModule: false
    };

    packages.testSome(module.packages, options, done);
}

/**
 * Gets a reference to a module object by using the module name or alias.
 *
 * @param {String} moduleNameOrAlias The name of the module or its alias
 * @return {Object} the reference to the package object
 */
function getModule(moduleNameOrAlias) {

    var module,
        moduleName = moduleNameOrAlias;

    if (!config.modules[moduleName]) {
        moduleName = config.moduleAliases[moduleName];
    }

    module = config.modules[moduleName];

    if (!module) {
        throw new Error(('Module [' + moduleNameOrAlias + '] doesn`t exist').red);
    }

    return module;
}


/**
 * Builds the module (js, locale, css).
 *
 * @param {String} moduleNameOrAlias The name of the module or its alias
 * @param {Object} options
 * @param {function(error)} done The function to execute when the run is complete
 */
function build(moduleNameOrAlias, options, done) {
    runAntForEachModule(moduleNameOrAlias, 'build', options, done);
}

/**
 * Builds the WAR file and copies it to the tomcat webapps folder.
 *
 * @param {String} moduleNameOrAlias The name of the module or its alias
 * @param {Object} options
 * @param {function(error)} done The function to execute when the run is complete
 */
function deploy(moduleNameOrAlias, options, done) {
    runAntForEachModule(moduleNameOrAlias, 'deploy', options, done);
}

/**
 * Builds and copy the JS/CSS files to the Tomcat directory.
 *
 * @param {String} moduleNameOrAlias The name of the module or its alias
 * @param {Object} options
 * @param {function(error)} done The function to execute when the run is complete
 */
function quickDeploy(moduleNameOrAlias, options, done) {
    runAntForEachModule(moduleNameOrAlias, 'quick-deploy', options, done);
}

/**
 * Builds and copy the JS files to the Tomcat directory.
 *
 * @param {String} moduleNameOrAlias The name of the module or its alias
 * @param {Object} options
 * @param {function(error)} done The function to execute when the run is complete
 */
function quickDeployJS(moduleNameOrAlias, options, done) {
    runAntForEachModule(moduleNameOrAlias, 'quick-deploy-js', options, done);
}

/**
 * Builds and copy the CSS files to the Tomcat directory.
 *
 * @param {String} moduleNameOrAlias The name of the module or its alias
 * @param {Object} options
 * @param {function(error)} done The function to execute when the run is complete
 */
function quickDeployCSS(moduleNameOrAlias, options, done) {
    runAntForEachModule(moduleNameOrAlias, 'quick-deploy-css', options, done);
}

/**
 * Performs the JS lint operation.
 *
 * @param {String} [moduleNameOrAlias=null] The name of the module or its alias
 * @param {Object} options
 * @param {function(error)} done The function to execute when the run is complete
 */
function lint(moduleNameOrAlias, options, done) {
    runAntForEachModule(moduleNameOrAlias, 'js-lint', options, done);
}

/**
 * Makes the WAR file in production mode.
 *
 * @param {String} [moduleNameOrAlias=null] The name of the module or its alias
 * @param {Object} options
 * @param {function(error)} done The function to execute when the run is complete
 */
function war(moduleNameOrAlias, options, done) {

    options = _.defaults({
        'app.scope': 'PRODUCTION'
    }, options);

    runAntForEachModule(moduleNameOrAlias, 'make-war', options, done);
}

/**
 * Removes the files created during the build process.
 *
 * @param {String} [moduleNameOrAlias=null] The name of the module or its alias
 * @param {Object} options
 * @param {function(error)} done The function to execute when the run is complete
 */
function clean(moduleNameOrAlias, options, done) {
    runAntForEachModule(moduleNameOrAlias, 'clean', options, done);
}

/**
 * Build the locale packages.
 *
 * @param {String} [moduleNameOrAlias=null] The name of the module or its alias
 * @param {Object} options
 * @param {function(error)} done The function to execute when the run is complete
 */
function locale(moduleNameOrAlias, options, done) {
    runAntForEachModule(moduleNameOrAlias, 'js-locale-package', options, done);
}

/**
 * Fetches the truststore file from the remote server.
 *
 * @param {function(error)} done The function to execute when the run is complete
 */
function fetchTruststore(done) {
    util.runAnt(['-f', config.antMasterBuildFile, 'fetch-truststore'], done);
}

/**
 * Configures the Truststore with MSD certificate.
 *
 * @param {function(error)} done The function to execute when the run is complete
 */
function configureMsdTrustore(done) {
    util.runAnt(['-f', config.antMSDBuildFile, 'configure-truststore'], done);
}

/**
 * Executes Tomcat Run command.
 *
 * @param {function(error)} done The function to execute when the run is complete
 */
function execTomcatRun(done) {
    var cmd = util.isWin() ? 'catalina.bat' : './catalina.sh';
    util.spawn(cmd, ['run'], {cwd: config.tomcatBinPath}).on('exit', function(code) {
        var err;
        if (code !== 0) {
            err = 'Ant exited with an error';
        }
        done(err);
    });
}

/**
 * Starts Tomcat.
 *
 * @param {Object} options
 * @param {function(error)} done The function to execute when the run is complete
 */
function startTomcat(options, done) {
    fetchTruststore(function(err) {
        if (!err) {
            if (options.msd) {
                configureMsdTrustore(function(err) {
                    if (!err) {
                        execTomcatRun(done);
                    }
                })
            } else {
                execTomcatRun(done);
            }
        }
    });
}

/**
 * Deploys OneUI to Tomcat.
 *
 * @param {function(error)} done The function to execute when the run is complete
 */
function deployOneUI(done) {
    util.runAnt(['-f', config.antMasterBuildFile, 'oneui-install'], done);
}

/**
 * Deploys WIRIS to Tomcat.
 *
 * @param {function(error)} done The function to execute when the run is complete
 */
function deployWIRIS(done) {
    util.runAnt(['-f', config.antMasterBuildFile, 'wiriseditor-deploy-war'], done);
}

/**
 * Runs a specific ANT target.
 *
 * @param {String} [moduleNameOrAlias=null] The name of the module or its alias
 * @param {Object} options
 * @param {function(error)} done The function to execute when the run is complete
 */
function runAntTarget(moduleNameOrAlias, options, done) {
    var runAntTarget = options.target;
    delete options.target;
    runAntForEachModule(moduleNameOrAlias, runAntTarget, options, done);
}

/**
 * Runs ANT for each module or for the one specified in the parameters
 * @param {String} [moduleNameOrAlias] The name of the module fo which ANT should run. If missing, all modules will be used
 * @param {String} target The ANT target to run
 * @param {Object} options
 * @param {function(error)} done The function to execute when the run is complete
 */
function runAntForEachModule(moduleNameOrAlias, target, options, done) {

    var moduleNames;

    if (moduleNameOrAlias) {
        moduleNames = [moduleNameOrAlias];
    } else {
        moduleNames = Object.keys(config.modules);
    }

    async.eachSeries(moduleNames, function(moduleName, seriesCallback) {
        runAnt(moduleName, target, options, function(err) {
            seriesCallback(err);
        });
    }, function(err) {
        done(err);
    });
}

/**
 * Runs the ANT build script of the module using the given ANT target and options.
 *
 * @param {String} moduleNameOrAlias The name of the module or its alias
 * @param {String} target The ANT target to run
 * @param {Object} options Name/Value collection of ANT parameters used when executing the ANT target 
 * @param {function(error)} done The function to execute when the run is complete
 */
function runAnt(moduleNameOrAlias, target, options, done) {

    var module = getModule(moduleNameOrAlias),
        antBuildFile,
        antOptions,
        moreOptions;

    antBuildFile = path.join(config.rootDir, module.name, module.antBuildFile || (module.name + '-build.xml'));
    antOptions = ['-f', antBuildFile, target];

    moreOptions = _.defaults({}, options, {
        'app.scope': config.appScope
    });

    moreOptions = _.map(moreOptions, function(value, key) {
        return '-D' + key + '=' + value;
    });

    util.runAnt(antOptions.concat(moreOptions), done);
}


module.exports = {
    test: test,
    build: build,
    deploy: deploy,
    quickDeploy: quickDeploy,
    quickDeployJS: quickDeployJS,
    quickDeployCSS: quickDeployCSS,
    lint: lint,
    war: war,
    clean: clean,
    locale: locale,
    startTomcat: startTomcat,
    deployOneUI: deployOneUI,
    deployWIRIS: deployWIRIS,
    runAntTarget: runAntTarget,
    getModule: getModule
};