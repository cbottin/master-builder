var through = require('through2').obj,
    istanbul = require('istanbul'),
    instrumenter = new istanbul.Instrumenter(),
    path = require('path');

module.exports = function (opts) {

    return through(function (file, enc, cb) {

        //TODO Use Glup Util PluginError

        //if (!(file.contents instanceof Buffer)) {
        //    return cb(new PluginError(PLUGIN_NAME, 'streams not supported'));
        //}

        var filePath = file.path;

        if (opts.base) {
            filePath = path.relative(path.resolve(opts.base), filePath);
        }

        if (process.platform === 'win32') {
            filePath = filePath.replace(/\\/g, '/');
        }

        instrumenter.instrument(file.contents.toString(), './' + filePath, function (err, code) {
            //if (err) {
            //    return cb(new PluginError(
            //        PLUGIN_NAME,
            //        'Unable to parse ' + file.path + '\n\n' + err.message + '\n'
            //    ));
            //}

            file.contents = new Buffer(code);

            return cb(err, file);
        });
    });

};