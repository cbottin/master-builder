var _ = require('lodash');

//Are we running on Windows?
function isWin() {
    return /^win/.test(process.platform);
}

//Are we running on Linux?
function isLinux() {
    return /^linux/.test(process.platform);
}

//Are we running on Mac?
function isMac() {
    return /^darwin/.test(process.platform);
}

function runAnt(cmdArgs, done) {

    console.log('Starting ANT with:', cmdArgs);

    return spawn('ant', cmdArgs).on('exit', function(code) {
        var err;
        if (code !== 0) {
            err = 'Ant exited with an error';
        }
        done(err);
    });
}

//Spawn a child process
function spawn(cmd, args, options) {
    if (_.isString(args)) {
        args = args.split(/\s+/);
    }
    if (isWin()) {
        args = ['/c', cmd].concat(args);
        cmd = 'cmd';
    }
    var childProcess = require('child_process');
    var child = childProcess.spawn(cmd, args, options);
    child.stdout.pipe(process.stdout);
    child.stderr.pipe(process.stderr);
    return child;
}

module.exports = {
    isWin: isWin,
    isLinux: isLinux,
    isMac: isMac,
    runAnt: runAnt,
    spawn: spawn
};