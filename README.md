# Master Builder #

A nodeJS package to provide build tools for Thunderhead client-side repositories such as Communicate and Bulk.


## Gulp Tasks


### Build all Packages
`> gulp build`

### Build all Packages inside the Module
`> gulp build -m moduleNameOrAlias`

### Build all the locale files
`> gulp locale`

### Build the locale files for the Module
`> gulp locale -m moduleNameOrAlias`

### Deploy all WAR files to tomcat
`> gulp deploy`

### Deploy the WAR file for the Module to Tomcat
`> gulp deploy -m moduleNameOrAlias`

### Build and quick deploy to tomcat all files
`> gulp qd`

### Build and quick deploy to tomcat all files related to the Module 
`> gulp qd -m moduleNameOrAlias`

### Build and quick deploy to tomcat all JS files
`> gulp qdjs`

### Build and quick deploy to tomcat all JS files related to the Module 
`> gulp qdjs -m moduleNameOrAlias`

### Build and quick deploy to tomcat all CSS files
`> gulp qdcss`

### Build and quick deploy to tomcat all CSS files related to the Module 
`> gulp qdcss -m moduleNameOrAlias`

### Performs JS Lint against all Packages
`> gulp lint`

### Performs JS Lint against all Packages inside the Module
`> gulp lint -m moduleNameOrAlias`

### Make all Production WAR files
`> gulp war`

### Make Production WAR file for the Module
`> gulp war -m moduleNameOrAlias`

### Clean the build folders
`> gulp clean`

### Clean the build folders related to the Module
`> gulp clean -m moduleNameOrAlias`

### Start Tomcat
`> gulp tomcat`


### Start Tomcat with MSD certificate
`> gulp tomcat -m`

`> gulp tomcat --msd`

### Deploy ONEUI to Tomcat (temporary, will be removed soon)
`> gulp oneui`

### Deploy WIRIS to Tomcat
`> gulp wiris`

### Run a specific ANT target for all Modules
`> gulp ant -t targetName`

### Run a specific ANT target for the Module
`> gulp ant -t targetName -m moduleNameOrAlias`
