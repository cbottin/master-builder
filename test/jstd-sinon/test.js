describe('jstd.sinon', function() {

    describe('jstd.sinon.spy', function() {

        var sinonRef = window.sinon;

        it('throws errors if targeted properties are not null or functions', function () {
            window.sinon = {
                spy: function() {}
            };
            var obj = {
                arrayProp: [],
                stringProp: 'a',
                functionProp: function () {
                },
                nullProp: null
            };

            expect(function () { jstd.sinon.spy(obj, 'arrayProp'); }).toThrowError('Attempted jstd.sinon.spy of property "arrayProp" - which is not a function, is type [array]');
            expect(function () { jstd.sinon.spy(obj, 'stringProp'); }).toThrowError('Attempted jstd.sinon.spy of property "stringProp" - which is not a function, is type [string]');
            // Allow spying of functions and null properties
            jstd.sinon.spy(obj, 'functionProp');
            jstd.sinon.spy(obj, 'nullProp');
        });

        afterEach(function() {
            window.sinon = sinonRef;
        });

    });

    describe('jstd.sinon.stub', function() {

        var sinonRef = window.sinon;

        it('throws errors if targeted properties are not null or functions', function () {
            window.sinon = {
                stub: function() {}
            };
            var obj = {
                arrayProp: [],
                stringProp: 'a',
                functionProp: function () {
                },
                nullProp: null
            };

            expect(function () { jstd.sinon.stub(obj, 'arrayProp'); }).toThrowError('Attempted jstd.sinon.stub of property "arrayProp" - which is not a function, is type [array]');
            expect(function () { jstd.sinon.stub(obj, 'stringProp'); }).toThrowError('Attempted jstd.sinon.stub of property "stringProp" - which is not a function, is type [string]');
            // >2 arguments still produces correct error
            expect(function () { jstd.sinon.stub(obj, 'stringProp', function() {} ); }).toThrowError('Attempted jstd.sinon.stub of property "stringProp" - which is not a function, is type [string]');
            // Allow spying of functions and null properties
            jstd.sinon.stub(obj, 'functionProp');
            jstd.sinon.stub(obj, 'nullProp');
        });

        afterEach(function() {
            window.sinon = sinonRef;
        });

    });

});