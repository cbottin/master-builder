describe('jstd.stub', function() {

    it('works when using single properties', function() {

        var functionA = function() {};
        var newFunctionA = function() {};
        var propertyA = 'value';
        var newPropertyA = 'newValue';
        var propertyB = {};
        var newPropertyB = {};

        var obj = {
            'propertyA': 'value',
            'propertyB': propertyB,
            'functionA': functionA
        };

        jstd.stub.apply(obj, 'propertyA', newPropertyA);

        expect(obj.propertyA).toBe(newPropertyA);
        expect(obj.propertyB).toBe(propertyB);
        expect(obj.functionA).toBe(functionA);

        jstd.stub.apply(obj, 'propertyB', newPropertyB);

        expect(obj.propertyA).toBe(newPropertyA);
        expect(obj.propertyB).toBe(newPropertyB);
        expect(obj.functionA).toBe(functionA);


        jstd.stub.apply(obj, 'functionA', newFunctionA);

        expect(obj.propertyA).toBe(newPropertyA);
        expect(obj.propertyB).toBe(newPropertyB);
        expect(obj.functionA).toBe(newFunctionA);

        expect(jstd.stub.original(obj, 'propertyA')).toBe(propertyA);
        expect(jstd.stub.original(obj, 'propertyB')).toBe(propertyB);
        expect(jstd.stub.original(obj, 'functionA')).toBe(functionA);

        jstd.stub.restore();

        expect(obj.propertyA).toBe(propertyA);
        expect(obj.propertyB).toBe(propertyB);
        expect(obj.functionA).toBe(functionA);

        expect(jstd.stub.original(obj, 'propertyA')).toBe(undefined);
        expect(jstd.stub.original(obj, 'propertyB')).toBe(undefined);
        expect(jstd.stub.original(obj, 'functionA')).toBe(undefined);
    });

    it ('compares types of the stubbed properties', function() {
        var newObjectProp = {};

        var obj = {
            arrayProp: [],
            objectProp: {},
            functionProp: function() {},
            stringProp: '',
            booleanProp: false,
            numberProp: 123,
            nullProp: null
        };

        expect(function() { jstd.stub.apply(obj, 'undefinedProp', ''); }).toThrowError('Attempted jstd.stub of undefined property [undefinedProp]');
        expect(function() { jstd.stub.apply(obj, 'arrayProp', {}); }).toThrowError('Attempted jstd.stub of [array] property "arrayProp" with type [object]');
        expect(function() { jstd.stub.apply(obj, 'objectProp', []); }).toThrowError('Attempted jstd.stub of [object] property "objectProp" with type [array]');
        expect(function() { jstd.stub.apply(obj, 'functionProp', {}); }).toThrowError('Attempted jstd.stub of [function] property "functionProp" with type [object]');
        expect(function() { jstd.stub.apply(obj, 'stringProp', false); }).toThrowError('Attempted jstd.stub of [string] property "stringProp" with type [boolean]');
        expect(function() { jstd.stub.apply(obj, 'booleanProp', ''); }).toThrowError('Attempted jstd.stub of [boolean] property "booleanProp" with type [string]');
        expect(function() { jstd.stub.apply(obj, 'numberProp', function(){}); }).toThrowError('Attempted jstd.stub of [number] property "numberProp" with type [function]');
        // Allow overriding of null properties with any type
        jstd.stub.apply(obj, 'nullProp', newObjectProp);
        expect(obj.nullProp).toBe(newObjectProp);
        jstd.stub.apply(obj, 'functionProp', null);
        expect(obj.functionProp).toBeNull();
    });

    it('works when using multiple properties', function() {

        var functionA = function() {};
        var newFunctionA = function() {};
        var propertyA = 'value';
        var newPropertyA = 'newValue';
        var propertyB = {};
        var newPropertyB = {};

        var obj = {
            'propertyA': 'value',
            'propertyB': propertyB,
            'functionA': functionA
        };

        jstd.stub.apply(obj, {
            'propertyA': newPropertyA,
            'propertyB': newPropertyB,
            'functionA': newFunctionA
        });

        expect(obj.propertyA).toBe(newPropertyA);
        expect(obj.propertyB).toBe(newPropertyB);
        expect(obj.functionA).toBe(newFunctionA);

        expect(jstd.stub.original(obj, 'propertyA')).toBe(propertyA);
        expect(jstd.stub.original(obj, 'propertyB')).toBe(propertyB);
        expect(jstd.stub.original(obj, 'functionA')).toBe(functionA);

        jstd.stub.restore();

        expect(obj.propertyA).toBe(propertyA);
        expect(obj.propertyB).toBe(propertyB);
        expect(obj.functionA).toBe(functionA);

        expect(jstd.stub.original(obj, 'propertyA')).toBe(undefined);
        expect(jstd.stub.original(obj, 'propertyB')).toBe(undefined);
        expect(jstd.stub.original(obj, 'functionA')).toBe(undefined);
    });

    describe("jstd.stub.restore", function() {

        var obj1Props = {
            propertyA: '',
            propertyB: {}
        };

        var obj1NewProps = {
            propertyA: 'A',
            propertyB: {}
        };

        var obj2Props = {
            propertyA: true,
            propertyB: null
        };

        var obj2NewProps = {
            propertyA: false,
            propertyB: ''
        };

        var obj1 = {
            'propertyA': obj1Props.propertyA,
            'propertyB': obj1Props.propertyB
        };

        var obj2 = {
            'propertyA': obj2Props.propertyA,
            'propertyB': obj2Props.propertyB
        };

        it('works when restoring all targets', function() {

            jstd.stub.apply(obj1, {
                'propertyA': obj1NewProps.propertyA,
                'propertyB': obj1NewProps.propertyB
            });

            jstd.stub.apply(obj2, {
                'propertyA': obj2NewProps.propertyA,
                'propertyB': obj2NewProps.propertyB
            });

            expect(obj1.propertyA).toBe(obj1NewProps.propertyA);
            expect(obj1.propertyB).toBe(obj1NewProps.propertyB);
            expect(obj2.propertyA).toBe(obj2NewProps.propertyA);
            expect(obj2.propertyB).toBe(obj2NewProps.propertyB);

            jstd.stub.restore();

            expect(obj1.propertyA).toBe(obj1Props.propertyA);
            expect(obj1.propertyB).toBe(obj1Props.propertyB);
            expect(obj2.propertyA).toBe(obj2Props.propertyA);
            expect(obj2.propertyB).toBe(obj2Props.propertyB);

        });

        it('works when restoring individual property', function() {

            jstd.stub.apply(obj1, {
                'propertyA': obj1NewProps.propertyA,
                'propertyB': obj1NewProps.propertyB
            });

            jstd.stub.apply(obj2, {
                'propertyA': obj2NewProps.propertyA,
                'propertyB': obj2NewProps.propertyB
            });

            expect(obj1.propertyA).toBe(obj1NewProps.propertyA);
            expect(obj1.propertyB).toBe(obj1NewProps.propertyB);
            expect(obj2.propertyA).toBe(obj2NewProps.propertyA);
            expect(obj2.propertyB).toBe(obj2NewProps.propertyB);

            jstd.stub.restore(obj1, 'propertyA');

            expect(obj1.propertyA).toBe(obj1Props.propertyA);
            expect(obj1.propertyB).toBe(obj1NewProps.propertyB);
            expect(obj2.propertyA).toBe(obj2NewProps.propertyA);
            expect(obj2.propertyB).toBe(obj2NewProps.propertyB);

            jstd.stub.restore(obj1, 'propertyB');

            expect(obj1.propertyA).toBe(obj1Props.propertyA);
            expect(obj1.propertyB).toBe(obj1Props.propertyB);
            expect(obj2.propertyA).toBe(obj2NewProps.propertyA);
            expect(obj2.propertyB).toBe(obj2NewProps.propertyB);

        });


        it('works when restoring a specific target', function() {

            jstd.stub.apply(obj1, {
                'propertyA': obj1NewProps.propertyA,
                'propertyB': obj1NewProps.propertyB
            });

            jstd.stub.apply(obj2, {
                'propertyA': obj2NewProps.propertyA,
                'propertyB': obj2NewProps.propertyB
            });

            expect(obj1.propertyA).toBe(obj1NewProps.propertyA);
            expect(obj1.propertyB).toBe(obj1NewProps.propertyB);
            expect(obj2.propertyA).toBe(obj2NewProps.propertyA);
            expect(obj2.propertyB).toBe(obj2NewProps.propertyB);

            jstd.stub.restore(obj1);

            expect(obj1.propertyA).toBe(obj1Props.propertyA);
            expect(obj1.propertyB).toBe(obj1Props.propertyB);
            expect(obj2.propertyA).toBe(obj2NewProps.propertyA);
            expect(obj2.propertyB).toBe(obj2NewProps.propertyB);

            jstd.stub.restore(obj2);

            expect(obj1.propertyA).toBe(obj1Props.propertyA);
            expect(obj1.propertyB).toBe(obj1Props.propertyB);
            expect(obj2.propertyA).toBe(obj2Props.propertyA);
            expect(obj2.propertyB).toBe(obj2Props.propertyB);

        });

    });

});