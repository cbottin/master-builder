var path = require('path'),
    yargs = require('yargs'),
    modules = require('./lib/modules/index.js'),
    packages = require('./lib/packages/index.js'),
    packageFiles = require('./lib/packages/files.js'),
    testCoverage = require('./lib/test/coverage/index.js'),
    configManager = require('./lib/config.js'),
    config = configManager.config,
    gulp = configManager.getGulp() || require('gulp');


/**
 * Instruments all the packages inside a module to the given destination
 */
gulp.task('instrument-module', function instrumentModule(done) {

    var argv = yargs.require('name')
        .argv;

    var concat = require('gulp-concat'),
        async = require('async'),
        instrumenter = require('./lib/gulp-plugins/istanbul-instrumenter.js'),
        module = modules.getModule(argv.name);

    async.each(module.packages, function(pkgName, callback) {
        var pkg = config.packages[pkgName];
        if (pkg.src) {
            var output = path.parse(pkg.output);
            packageFiles.getPackageSourceFilesInOrder(pkg, function (files) {
                gulp.src(files, {base: config.rootDir})
                    .pipe(instrumenter({base: config.rootDir}))
                    .pipe(concat(output.base + '.coverage'))
                    .pipe(gulp.dest(output.dir))
                    .on('end', function() {
                        callback();
                    });
            });
        }
    }, function(err) {
        done(err);
    });
});


/**
 *  Runs the unit tests for a package or a module
 *
 *  > gulp test --module {{module name}}
 *  > gulp test --package {{package name}}
 *  > gulp test --package {{package name}} --watch
 */
gulp.task('test', function test(done) {

    var argv = yargs.alias('module', 'm')
        .alias('package', 'p')
        .alias('watch', 'w')
        .alias('all', 'a')
        .alias('grep', 'g')
        .alias('nocov', 'nc')
        .boolean('watch')
        .boolean('nocov')
        .boolean('all')
        .boolean('failonerror')
        .boolean('generatemaps')
        .argv;

    var failOnError = argv.failonerror;
    var generateMaps = argv.generatemaps;
    var noCoverage = argv.nocov;

    if (argv.module) {
        modules.test(argv.module, {generateMaps: generateMaps, failOnError: failOnError, noCoverage: noCoverage}, function(error) {
            done(error && failOnError ? 'Some tests have failed. Please check the logs above': '');
        });
    } else if (argv.package) {

        var grep = argv.grep,
            args;

        if (grep) {
            args = ['--grep', grep];
        }

        packages.test(argv.package, {generateMaps: generateMaps, noCoverage: noCoverage, autoWatch: argv.watch, client: {args: args}}, function(error) {
            done(error && failOnError ? 'Some tests have failed. Please check the logs above': '');
        });
    } else if (argv.all) {
        packages.testAll({generateMaps: generateMaps, failOnError: failOnError, noCoverage: noCoverage}, function(error) {
            done(error && failOnError ? 'Some Tests have failed. Please check the logs above': '');
        });
    } else {
        throw new Error('Invalid arguments'.red);
    }
});


/**
 * Creates a coverage report from the given data at the given destination
 */
gulp.task('create-coverage-report', function(done) {

    var argv = yargs.require('src')
        .require('data')
        .require('dest')
        .array('data')
        .argv;

    var glob = require('glob'),
        async = require('async'),
        dataFiles = [],
        data = argv.data,
        destination = argv.dest,
        sourceDir = argv.src;

    // For each data, get the list of  files matching **/*.json
    async.each(data, function(dataPath, callback) {

        dataPath = path.extname(dataPath) === '.json' ? dataPath : path.join(dataPath, '**/*.json');

        glob(dataPath, function (err, files) {
            if (err) {
                callback(err);
            } else {
                dataFiles = dataFiles.concat(files);
                callback();
            }
        });

    }, function(err) {
        if (err) {
            throw err; //TODO Use Gulp Util PluginError
        } else {
            testCoverage.createReport(dataFiles, destination, sourceDir, done);
        }
    });
});


/**
 * @see modules#build
 */
gulp.task('build', function(done) {
    var argv = yargs
        .alias('module', 'm')
        .argv;

    modules.build(argv.module, {type: argv.type}, done);
});

/**
 * @see modules#deploy
 */
gulp.task('deploy', function(done) {
    var argv = yargs
        .alias('module', 'm')
        .argv;

    modules.deploy(argv.module, null, done);
});

/**
 * @see modules#quickDeployJS
 */
gulp.task('qdjs', function(done) {
    var argv = yargs
        .alias('module', 'm')
        .argv;

    modules.quickDeployJS(argv.module, null, done);
});

/**
 * @see modules#quickDeployCSS
 */
gulp.task('qdcss', function(done) {
    var argv = yargs
        .alias('module', 'm')
        .argv;

    modules.quickDeployCSS(argv.module, null, done);
});

/**
 * @see modules#quickDeploy
 */
gulp.task('qd', function(done) {
    var argv = yargs
        .alias('module', 'm')
        .argv;

    modules.quickDeploy(argv.module, null, done);
});

/**
 * @see modules#lint
 */
gulp.task('lint', function(done) {
    var argv = yargs
        .alias('module', 'm')
        .argv;

    modules.lint(argv.module, null, done);
});

/**
 * @see modules#war
 */
gulp.task('war', function(done) {
    var argv = yargs
        .alias('module', 'm')
        .argv;

    modules.war(argv.module, {all: argv.all}, done);
});

/**
 * @see modules#clean
 */
gulp.task('clean', function(done) {
    var argv = yargs
        .alias('module', 'm')
        .argv;

    modules.clean(argv.module, null, done);
});

/**
 * @see modules#locale
 */
gulp.task('locale', function(done) {
    var argv = yargs
        .alias('module', 'm')
        .argv;

    modules.locale(argv.module, null, done);
});


/**
 * @see modules#startTomcat
 */
gulp.task('tomcat', function(done) {
    var argv = yargs
        .alias('msd', 'm')
        .boolean('msd')
        .argv;

    modules.startTomcat({msd: argv.msd}, done);
});


/**
 * @see modules#deployOneUI
 */
gulp.task('oneui', function(done) {
    modules.deployOneUI(done);
});

/**
 * @see modules#deployWIRIS
 */
gulp.task('wiris', function(done) {
    modules.deployWIRIS(done);
});



/**
 * @see modules#runAntTarget
 */
gulp.task('ant', function(done) {
    var argv = yargs
        .alias('module', 'm')
        .alias('target', 't')
        .require('target')
        .argv;

    modules.runAntTarget(argv.module, {target: argv.target}, done);
});